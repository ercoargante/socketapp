package edu.avans.socketapp;

import java.net.*;
import java.util.Locale;

import android.util.Log;

public class SocketServerUDP {

    private static final String TAG = "(socket server udp)";

    public static String provideService(int serverPort, BooleanMutable serverRunning, SocketActivity_AsyncTaskSolution.NetworkTask nt) {
        try {
            DatagramSocket serverSocket = new DatagramSocket(serverPort);
            // allowing to interrupt the accept()
            // http://stackoverflow.com/questions/13967255/closing-additional-threads-during-serversocket-accept-in-android
            // http://stackoverflow.com/questions/6120986/close-listening-serversocket
            serverSocket.setSoTimeout(1000);
            String actionResult = "SocketServerUDP on (*, " + serverPort + ") is ready to receive capitalization requests.";
            Log.i(TAG, actionResult);
            nt.myPublishProgress(actionResult);

            byte[] receiveData = new byte[1024];
            byte[] sendData  = new byte[1024];

            while(serverRunning.get()) {
                try {
                    DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                    serverSocket.receive(receivePacket);
                    int receivedPacketLength = receivePacket.getLength();
                    InetAddress clientIpAddress = receivePacket.getAddress();
                    int clientPort = receivePacket.getPort();
                    String sentence = new String(receivePacket.getData());
                    String capitalizedSentence = sentence.toUpperCase(Locale.ENGLISH);
                    sendData = capitalizedSentence.getBytes();
                    DatagramPacket sendPacket = new DatagramPacket(sendData, receivedPacketLength, clientIpAddress, clientPort);
                    serverSocket.send(sendPacket);
                    actionResult = "SocketServerUDP has delivered the capitalization service to ("
                            + clientIpAddress + ", " + clientPort + ").";
                    Log.i(TAG, actionResult);
                    nt.myPublishProgress(actionResult);
                } catch (SocketTimeoutException e) {
                    // You don't really need to handle this
                    // Log.i(TAG, "Exception: " + e);

                }
            }
            serverSocket.close();
            Log.i(TAG, "SocketServerUDP has stopped");
            return "SocketServerUDP has stopped";
        } catch (Exception e) {
        	Log.i(TAG, "Exception: " + e);
            return "Exception: " + e;
        }
    }
}

