package edu.avans.socketapp;

import java.lang.ref.WeakReference;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

public class VH8I_SocketPracticumActivity_HandlerSolution extends Activity {

    private static final int REFRESH = 1;
    private static final String TAG = "(socket practicum activity)";
    private Context ctx;
    private String modifiedSentence;

    private GuiRefresher guiRefresher = new GuiRefresher(this); 
   
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ctx = this;

        (new Thread() {
            public void run() {
            	//modifiedSentence = SocketClientUDP.consumeService();
            	modifiedSentence = SocketClientTCP.consumeService("TODO", 123456);
            	Message message = Message.obtain(guiRefresher, REFRESH, modifiedSentence);
            	guiRefresher.sendMessage(message);
	            
	            //SocketServerTCP.provideService();
	            //SocketServerUDP.provideService();
        	}
       	}
      	).start();              
    }

    static class GuiRefresher extends Handler  {
    	
		private final WeakReference<VH8I_SocketPracticumActivity_HandlerSolution> mVH8I_SocketPracticumActivity_HandlerSolution;

		GuiRefresher(VH8I_SocketPracticumActivity_HandlerSolution vH8I_SocketPracticumActivity_HandlerSolution) {
			mVH8I_SocketPracticumActivity_HandlerSolution = new WeakReference<VH8I_SocketPracticumActivity_HandlerSolution>(
					vH8I_SocketPracticumActivity_HandlerSolution);
		}
		
    	@Override
    	public void handleMessage(Message msg) {
    		VH8I_SocketPracticumActivity_HandlerSolution vH8I_SocketPracticumActivity_HandlerSolution = mVH8I_SocketPracticumActivity_HandlerSolution.get();
    	switch(msg.what){
    	     case REFRESH:
    	         /*Refresh UI*/
            	 TextView tv = new TextView(vH8I_SocketPracticumActivity_HandlerSolution.ctx);
        	     tv.setText("Hello, Android! The modified sentence is: " + (String)msg.obj);
        	     vH8I_SocketPracticumActivity_HandlerSolution.setContentView(tv);
    	    	 Log.i(TAG, "the modified sentence = " + (String) msg.obj);
    	         break;
    	    }
    	}
    };            
}

