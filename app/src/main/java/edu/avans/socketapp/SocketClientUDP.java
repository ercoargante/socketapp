package edu.avans.socketapp;

import java.net.*;

import android.util.Log;

public class SocketClientUDP {
    private static final String TAG = "(socket client udp)";

    
    public static String consumeService(String ipAddress, int port) {
    	//String ipAddress = "localhost"; // does not work
        // String ipAddress = "127.0.0.1"; // does not work
        //  String ipAddress = "10.0.0.13"; // works
    	//String ipAddress = "10.0.2.2"; // works
        // String ipAddress = "145.48.224.156"; // works
         
    	
        byte[] sendData = new String("send nice text").getBytes();
        byte[] receiveData = new byte[1024];
        String actionResult;
        DatagramSocket clientSocket = null;
        try {
            String modifiedSentence;
            actionResult = "SocketClientUDP will try to use capitalization service on (" + ipAddress + ", " + port + "); ";
            Log.i(TAG, actionResult);
            clientSocket = new DatagramSocket();
            // prevent waiting indefinitely on the receive()
            clientSocket.setSoTimeout(1000);
            String actionResult2 = "SocketClientUDP: socket has local (ephemeral) port " + clientSocket.getLocalPort() + "; ";
            Log.i(TAG, actionResult2);
            InetAddress iPAddress = InetAddress.getByName(ipAddress);
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, iPAddress, port);
            clientSocket.send(sendPacket);
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            clientSocket.receive(receivePacket);
            modifiedSentence = new String(receivePacket.getData(), 0, receivePacket.getLength());
            String actionResult3 = "SocketClientTCP: the modified sentence is: " + modifiedSentence;
            Log.i(TAG, actionResult3);
            actionResult += actionResult2;
            actionResult += actionResult3;
            clientSocket.close();
            return actionResult;
        } catch (SocketTimeoutException e) {
            // after timeout, the socket is still valid, so need to close explicitly
            if(clientSocket != null) {
                clientSocket.close();
            }
            actionResult = "Timeout! (exception: " + e + ")";
            Log.i(TAG, actionResult);
            return actionResult;
        } catch(Exception e) {
            actionResult = "Exception: " + e;
            Log.i(TAG, actionResult);
            return actionResult;
        }
    }

}
