package edu.avans.socketapp;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.ref.WeakReference;

// solution taken from http://developer.android.com/training/displaying-bitmaps/process-bitmap.html
public class SocketActivity_AsyncTaskSolution extends Activity {

    private static final String TAG = "(socket activity asynctasksolution)";
    private SocketActivity_AsyncTaskSolution thisActivity = this;
    private TextView selectActionSpinnerLabel;
    private Spinner selectActionSpinner;
    private Button startActionButton;
    private TextView actionResultTextView;
    private int selectedAction = 0;
    private EditText serverIpAddressEditText;
    private EditText serverPortEditText;
    private NetworkTask nt;
    private BooleanMutable serverRunning = new BooleanMutable();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_socket);

        final TextView deviceIpAddress = (TextView) findViewById(R.id.device_ip_address);
        deviceIpAddress.setText("IP address of this device is: " + IpUtils.getIPAddress(true));

        final TextView serverIpAddressLabel = (TextView) findViewById(R.id.server_ip_label);
        // initial visibility should be VISIBLE, as initial action is TCP client
        serverIpAddressLabel.setVisibility(View.VISIBLE);
        serverIpAddressEditText = (EditText) findViewById(R.id.server_ip);
        // initial visibility should be VISIBLE, as initial action is TCP client
        serverIpAddressEditText.setVisibility(View.VISIBLE);
        serverPortEditText = (EditText) findViewById(R.id.server_port);

        selectActionSpinnerLabel = (TextView) findViewById(R.id.action_spinner_label);

        selectActionSpinner = (Spinner) findViewById(R.id.action_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.actions_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectActionSpinner.setAdapter(adapter);
        // prevent the spinner from (1) firing at startup without user making a selection and
        // (2) prevent the spinner from firing more than once, if a selection is made.
        // http://stackoverflow.com/questions/2562248/how-to-keep-onitemselected-from-firing-off-on-a-newly-instantiated-spinner
        selectActionSpinner.setSelection(0, true);
        selectActionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.i(TAG, "choose selectActionSpinner option: " + (String) parent.getItemAtPosition(position));
                selectedAction = position;
                switch (selectedAction) {
                    case 0:
                        serverIpAddressEditText.setVisibility(View.VISIBLE);
                        serverIpAddressLabel.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        serverIpAddressEditText.setVisibility(View.GONE);
                        serverIpAddressLabel.setVisibility(View.GONE);
                        break;
                    case 2:
                        serverIpAddressEditText.setVisibility(View.VISIBLE);
                        serverIpAddressLabel.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        serverIpAddressEditText.setVisibility(View.GONE);
                        serverIpAddressLabel.setVisibility(View.GONE);
                        break;
                    default:
                        break;
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {
                Log.i(TAG, "selectActionSpinner: onNothingSelected() has been called");
                // deliberately ignore
            }
        });


        final Button stopServerButton = (Button) findViewById(R.id.stop_server_button);
        stopServerButton.setVisibility(View.GONE);
        stopServerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // calling nt.cancel(true) does not kill a thread that has a socket waiting on
                // an accept(). Alternative solution is a timeout on accept
                // in combination with a boolean in the server while loop.
                //
                Log.i(TAG, "clicked stop server button");
                stopServerButton.setVisibility(View.GONE);
                serverRunning.setFalse();
                // boolean "false" means waiting for the async task to complete instead of sending
                //  a hard thread interrupt. So the only effects of adding the
                // statement "nt.cancel(false);" are:
                // - instead of onPostExecute() the method onCancelled() is called
                // - nt.isCancelled() will return true, whereas without the statement it returns false
                // so both with and without the "nt.cancel(false);" statement the application works
                // correctly
                // nt.cancel(false);
                Log.i(TAG, "server action cancelled is: " + nt.isCancelled());
            }
        });


        startActionButton = (Button) findViewById(R.id.start_action_button);
        startActionButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.i(TAG, "clicked start action button");
                switch (selectedAction) {
                    case 0:
                        selectActionSpinnerLabel.setVisibility(View.GONE);
                        selectActionSpinner.setVisibility(View.GONE);
                        startActionButton.setVisibility(View.GONE);
                        break;
                    case 1:
                        selectActionSpinnerLabel.setVisibility(View.GONE);
                        selectActionSpinner.setVisibility(View.GONE);
                        startActionButton.setVisibility(View.GONE);
                        stopServerButton.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        selectActionSpinnerLabel.setVisibility(View.GONE);
                        selectActionSpinner.setVisibility(View.GONE);
                        startActionButton.setVisibility(View.GONE);
                        break;
                    case 3:
                        selectActionSpinnerLabel.setVisibility(View.GONE);
                        selectActionSpinner.setVisibility(View.GONE);
                        startActionButton.setVisibility(View.GONE);
                        stopServerButton.setVisibility(View.VISIBLE);
                        break;
                    default:
                        break;
                }
                nt = new NetworkTask(thisActivity, selectActionSpinnerLabel, selectActionSpinner, startActionButton, actionResultTextView);
                nt.execute((Void[]) null);
            }
        });


        actionResultTextView = (TextView) findViewById(R.id.action_result);

    }
        
    public class NetworkTask extends AsyncTask<Void, String, String> {
        private final WeakReference<Activity> activityReference;

    	public NetworkTask(Activity activity, TextView actionSpinnerLabel, Spinner actionSpinner, Button startActionButton, TextView actionResultTextView) {
            // Use a WeakReference to ensure that the parent activity can be garbage collected
            // if a Java object is only referenced by weak references, it can be garbage collected
            activityReference = new WeakReference<Activity>(activity);
    	}
    	
    	@Override
    	protected String doInBackground(Void... v) {

            switch (selectedAction) {
                case 0:
                    return SocketClientTCP.consumeService(serverIpAddressEditText.getText().toString(),
                            Integer.parseInt(serverPortEditText.getText().toString()));
                case 1:
                    serverRunning.setTrue();
                    return SocketServerTCP.provideService(
                            Integer.parseInt(serverPortEditText.getText().toString()), serverRunning, this);
                case 2:
                    return SocketClientUDP.consumeService(serverIpAddressEditText.getText().toString(),
                            Integer.parseInt(serverPortEditText.getText().toString()));
                case 3:
                    serverRunning.setTrue();
                    return SocketServerUDP.provideService(
                            Integer.parseInt(serverPortEditText.getText().toString()), serverRunning, this);
                default: return "error";
            }
        }

        @Override
        protected void onPostExecute(String actionResult) {
            if(activityReference != null) {
                // create a strong reference before using the object
                final SocketActivity_AsyncTaskSolution activity = (SocketActivity_AsyncTaskSolution) activityReference.get();
                if (activity != null) {
                    activity.selectActionSpinnerLabel.setVisibility(View.VISIBLE);
                    activity.selectActionSpinner.setVisibility(View.VISIBLE);
                    activity.startActionButton.setVisibility(View.VISIBLE);
                    Utils.appendColoredText(activity.actionResultTextView, "Action result (post exec): ", Color.RED);
                    activity.actionResultTextView.append(actionResult + "\n");
                }
            }
        }

        @Override
        protected void onCancelled(String actionResult) {
            if (activityReference != null && actionResult != null) {
                // create a strong reference before using the object
                final SocketActivity_AsyncTaskSolution activity = (SocketActivity_AsyncTaskSolution) activityReference.get();
                if (activity != null) {
                    activity.selectActionSpinnerLabel.setVisibility(View.VISIBLE);
                    activity.selectActionSpinner.setVisibility(View.VISIBLE);
                    activity.startActionButton.setVisibility(View.VISIBLE);
                    Utils.appendColoredText(activity.actionResultTextView, "Action result (cancel exec): ", Color.RED);
                    activity.actionResultTextView.append(actionResult + "\n");
                }
            }
        }

        @Override
        protected void onProgressUpdate(String... actionResult) {
            if (activityReference != null && actionResult != null) {
                // create a strong reference before using the object
                final SocketActivity_AsyncTaskSolution activity = (SocketActivity_AsyncTaskSolution) activityReference.get();
                if (activity != null) {
                    Utils.appendColoredText(activity.actionResultTextView, "Action result (progress exec): ", Color.RED);
                    activity.actionResultTextView.append(actionResult[0] + "\n");
                }
            }
        }

        public void myPublishProgress(String actionResult) {
            publishProgress(actionResult);
        }
}

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_socket, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}