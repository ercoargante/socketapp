package edu.avans.socketapp;

import java.io.*;
import java.net.*;
import java.util.Locale;

import android.os.AsyncTask;
import android.util.Log;

public class SocketServerTCP {
    private static final String TAG = "(socket server tcp)";

    public static String provideService(int serverPort, BooleanMutable serverRunning, SocketActivity_AsyncTaskSolution.NetworkTask nt) {
        String clientSentence;
        String capitalizedSentence;
        try {
            ServerSocket listener = new ServerSocket(serverPort);
            // allowing to interrupt the accept()
            // http://stackoverflow.com/questions/13967255/closing-additional-threads-during-serversocket-accept-in-android
            // http://stackoverflow.com/questions/6120986/close-listening-serversocket
            listener.setSoTimeout(1000);
            String actionResult = "SocketServerTCP: listening on (" + IpUtils.getIPAddress(true) + ", " + serverPort + ") and ready to receive capitalization requests.";
            Log.i(TAG, actionResult);
            nt.myPublishProgress(actionResult);
            while(serverRunning.get()) {
                try {
                    Socket connectionSocket = listener.accept();
                    Log.i(TAG, "SocketServerTCP: socket has local port " + connectionSocket.getLocalPort());
                    Log.i(TAG, "SocketServerTCP: remote port " + connectionSocket.getPort());
                    BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
                    DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
                    clientSentence = inFromClient.readLine();
                    capitalizedSentence = clientSentence.toUpperCase(Locale.ENGLISH) + '\n';
                    outToClient.writeBytes(capitalizedSentence);
                    actionResult = "SocketServerTCP: has delivered the capitalization service to ("
                            + connectionSocket.getInetAddress() + ", " + connectionSocket.getPort() + ").";
                    Log.i(TAG, actionResult);
                    nt.myPublishProgress(actionResult);
                } catch (SocketTimeoutException e) {
                    // You don't really need to handle this
                    // Log.i(TAG, "Exception: " + e);
                }
            }
            listener.close();
            Log.i(TAG, "SocketServerTCP has stopped");
            return "SocketServerTCP has stopped";
        } catch (Exception e) {
            Log.i(TAG, "Exception: " + e);
            return "Exception: " + e;
        }
    }
}
