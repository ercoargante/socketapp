package edu.avans.socketapp;

/**
 * Created by Erco on 19-2-2015.
 */
public class BooleanMutable {
    private boolean value = false;

    public boolean get() {
        return value;
    }

    public void setTrue() {
        value = true;
    }

    public void setFalse() {
        value = false;
    }
}
