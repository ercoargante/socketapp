package edu.avans.socketapp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import android.util.Log;

public class SocketClientTCP {
    private static final String TAG = "(socket client tcp)";

    public static String consumeService(String ipAddress, int port) {
        //String ipAddress = "localhost"; does not work
        // String ipAddress = "127.0.0.1"; // also works
        // String ipAddress = "10.0.0.13"; // also works
        //String ipAddress = "10.0.2.2"; // also works

        String actionResult;
        try {
            String modifiedSentence;
            actionResult = "SocketClientTCP will try to use capitalization service on (" + ipAddress + ", " + port + "); ";
        	Log.i(TAG, actionResult);
            Socket socket = new Socket(ipAddress, port);
            String actionResult2 = "SocketClientTCP: socket has local (ephemeral) port " + socket.getLocalPort() + "; ";
            Log.i(TAG, actionResult2);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println("send nice text");
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            modifiedSentence = inFromServer.readLine();
            String actionResult3 = "SocketClientTCP: the modified sentence is: " + modifiedSentence;
            Log.i(TAG, actionResult3);
            actionResult += actionResult2;
            actionResult += actionResult3;
            socket.close();
            return actionResult;
        } catch(Exception e) {
            actionResult = "Exception: " + e;
        	Log.i(TAG, actionResult);
            return actionResult;
        }
    }

}